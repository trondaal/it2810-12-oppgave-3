//This code initializes the platform that the application runs in.
//It then uses the platform to bootstrap the AppModule
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);