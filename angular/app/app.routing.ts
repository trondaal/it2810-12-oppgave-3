//Routing hadles navigation between multiple views, in this case between the student and subject views.
//All the views are declared in Routes[] defining which component and path belongs to which view.

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentComponent } from  './student.component';
import { SubjectComponent } from './subject.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/subject',
        pathMatch: 'full'
    },
    {
        path: 'student',
        component: StudentComponent
    },
    {
        path: 'subject',
        component: SubjectComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);