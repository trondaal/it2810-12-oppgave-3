/*
This is the main component of our prototype. In this component the user can search
for subjects and add them to a list of favourite subjects. 
The results for the search can also be filtered depending on how many credits they give.
*/

import { Component } from '@angular/core';
import { style, state } from '@angular/core';
import { SubjectService } from './subject.service';
import { Subject } from './subject';
import { StudentComponent } from './student.component';
import { Router } from '@angular/router';

import 'rxjs/Rx';

@Component({
    selector: 'my-course',
    template: `
    <div class="container">
        <div class="col-md-7">
            <div class="row search">
                <div class='searchField col-md-12'>
                    <h3>Søk etter kurs:</h3>
                    <div class="input-group">
                        <input #courseCode class="form-control" type="text" (keydown.enter)="getSubject(courseCode.value)" placeholder="kurskode (f.eks tdt4100)"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" (click)="getSubject(courseCode.value)">Go!</button>
                        </span>
                    </div>
                    <div class="filter-group">
                        <p>Filtrer: </p>
                        <div>
                            <input #check15 type="checkbox" (change)="creditFilter('15', check15.checked)"><p class="inline"> 15 stp</p></div>
                            <input #check7 type="checkbox" (change)="creditFilter('7.5', check7.checked)"><p class="inline"> 7.5 stp</p>
                    </div>
                </div>
            </div><br>

	        <div id="favorites">
		        <h3>Favorite subjects</h3>
				<ul>
					<li *ngFor="let favsub of fav">{{ favsub.code }} - {{ favsub.name }}</li>
				</ul>
			</div>
			<div id="seach-result">
	            <div *ngIf='result && !hasFiltered'>
	                <div class="row" *ngFor="let subject of result">
	                    <div class="col-md-12">
	                        <h4>
	                        	{{subject.code}} - {{subject.name}} - {{subject.credit}} stp.
	                        	<button (click)="addFav(subject)">Fav</button>
								<button (click)="unFav(subject)">UnFav</button>
	                        </h4>
	                    </div>
	                </div>
	            </div>
	            <div *ngIf="(result && result.length == 0) || (hasFiltered && resultFiltered && resultFiltered.length == 0)">Det var ingen fagkoder som matchet søket ditt.</div>
	            <div *ngIf="result && hasFiltered">
	                <div class="row" *ngFor="let subject of resultFiltered">
	                    <div class="col-md-12">
	                        <h4>
	                        	{{subject.code}} - {{subject.name}} - {{subject.credit}} stp.
	                        	<button (click)="addFav(subject)">Fav</button>
								<button (click)="unFav(subject)">UnFav</button>
	                        </h4>
	                    </div>
	                </div>
	            </div>
			</div>
            
			
        </div>
        
    </div>
    `,
    providers: [SubjectService],
    styles: [`
        .list-group-item:nth-child(odd)
        {
            background-color: #E6F7FC;
        }
        .pointer
        {
            cursor: pointer;
        }
        h3{
            margin: 0px;
            padding: 1em 1em 1em 0em;    
        }
        .container{
            width: 100%
        }
        .row.search{
            background-color: #d9e5f2;
            padding: 0em 0em 0.5em 1em ;
        }
        li{
            font-family: 'Karla', sans-serif;
        }
        .inline{
            display: inline;

        }
    `],
})

export class SubjectComponent {

    result: Array<Subject>;
    resultFiltered: Array<Subject> = [];

    hasFiltered: boolean = false;

    oneFiltered: boolean = false;
    twoFiltered: boolean = false;

    constructor(private subjectService: SubjectService, private router: Router) { }

    /*
    This function retrieves all matches for 
    the user's search query using subject.service.ts
    */
    getSubject(subjectCode) {
        if (subjectCode.length > 0) {
            this.result = this.subjectService.getMatchingSubject(subjectCode);
        }
    }
    
    /*
    This function adds and subtracts subjects to/from the 'Favorite subjects' list
    */
    fav = [];
	addFav(s:Subject){
		console.log(JSON.stringify(s));
		this.fav.push(s);
	}
	unFav(s:Subject){
		var index = this.fav.indexOf(s);
		this.fav.splice(index, 1);		
	}

    /*
    This function lets the user filter his/her search results depending on credits.
    */
    creditFilter(credits: string, selected: boolean) {
        
        
        if(selected){
            for(var i = 0; i < this.result.length; i++){
                if (this.result[i].credit === credits){
                   this.resultFiltered.push(this.result[i]);
                }
            }
            this.hasFiltered = true;       

            if (credits === '15'){
                this.oneFiltered = true;
            }
            else{
                this.twoFiltered = true;
            }
        }
        else {
            if (credits === '15'){
                this.oneFiltered = false;
            }
            else{
                this.twoFiltered = false;
            }

            if (this.oneFiltered || this.twoFiltered){
                var tempResult: Subject[] = [];
                for (var i = 0; i < this.resultFiltered.length; i++){
                    if (this.resultFiltered[i].credit !== credits){
                        tempResult.push(this.resultFiltered[i]);
                    }
                }
                this.resultFiltered = tempResult;
            }
            else {
                this.resultFiltered = [];
                this.hasFiltered = false;
            }
        }
    }
}