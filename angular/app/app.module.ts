//This is the entry point to our application.
//It imports BrowserModule in order to run in a browser.
//It also imports all the other modules needed in the application. 
//Some of the modules are imported from @angular, while some are defined by us, the group.
import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent }   from './app.component';
import { StudentComponent } from './student.component';
import { SubjectComponent } from './subject.component';

import { SubjectService } from './subject.service';

import { routing } from './app.routing';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    StudentComponent,
    SubjectComponent,
  ],
  providers: [
  	SubjectService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
