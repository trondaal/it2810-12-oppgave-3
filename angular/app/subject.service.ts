import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import { Subject } from './subject';

export class ShortSubject{
    code: string;
    name: string;
	credit: string;
}

@Injectable()
export class SubjectService{
	private courseApiUrl: string = "http://www.ime.ntnu.no/api/course/";
	private allCourses: Array<ShortSubject>;

	constructor(private http: Http) {
		http
			.get(this.courseApiUrl + '-')
			.subscribe((response: Response) => {this.allCourses = response.json().course; console.log(response.json());});
	}

	getMatchingSubject(code:string): Array<ShortSubject>{
		code = code.toUpperCase();
		var results : ShortSubject[] = [];
		for(var i = 0; i < this.allCourses.length; i++){
			if(i % 2 == 0 ? this.allCourses[i].credit = '15' : this.allCourses[i].credit = '7.5')
			if(this.allCourses[i].code.search(code) != -1){
				results.push(this.allCourses[i]);
			}
		}
		return results;
	}
}
