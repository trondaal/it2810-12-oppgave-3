/*
This component is a skeleton for information about a single student. 
If we were to continue with angular, 
this would be the profile page of students registered in the database.
*/

import { Component } from '@angular/core';

import { style, state } from '@angular/core';

export class Student 
{
	id: number;
	name: string;
}

@Component({
  selector: 'my-student',
  template: ` 
   <div id="studentView"class='container'>
	<div class="jumbotron">
		<h4>Her kommer personlig info om studenten som er logget inn.</h4>
		<h3>{{student.name}}</h3>
		<div class="row">
			<div class="col-md-12">
				<label>Studentnummer: </label>{{student.id}}
			</div>
			<div class="col-md-4">
				<label>Navn: </label>
				<input class="form-control" [(ngModel)]="student.name" placeholder="navn">
			</div>
		</div>
		<br>
	</div>

</div>`,

})
export class StudentComponent
{
	title = "Online";

	student: Student = 
	{
		id: 1,
		name: "Ola Normann"
	}
}
