//This is the root component of our Angular application
//The view rendered is a av bar with navigaton possibilities to other pages. 
//This navigation is handled by app.routing.ts
import { Component } from '@angular/core';

@Component({
    selector: 'search',
    template: `
    	<h1>FagGuide</h1>
        <ul id="nav">
            <li><a routerLink="/student">Student</a></li>
            <li><a routerLink="/subject">Kurs</a></li>
        </ul>
        
        <router-outlet></router-outlet>`,
        styleUrls: ['./styles.css']
})

export class AppComponent {
    
   
}