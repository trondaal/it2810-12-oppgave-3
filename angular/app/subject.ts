//This is the class type of the subjets listed in the search results
export class Subject{
    code: string;
    name: string;
	credit: string;
}