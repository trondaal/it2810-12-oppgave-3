var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubjectSchema = new Schema({
  code: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  credit: {
    type: Number,
    required: true,
    default: 7.5
  },
  studyLevel: {
    type: Number,
    required: true
  },
  taughtInEnglish: {
    type: Boolean,
    default: false
  },
  taughtInSpring: {
    type: Boolean,
    default: false
  },
  taughtInAutumn: {
    type: Boolean,
    default: false
  },
  location: {
    type: String
  }
});

SubjectSchema.methods.calcStudyLevel = function() {
  var studyLevel = this.studyLevel;
  if(studyLevel) {
    switch (studyLevel) {
      case 100:
        return 'Grunnleggende';
        break;
      case 200:
        return 'Videregående';
        break;
      case 300:
        return 'Tredjeårsemne';
        break;
      case 500:
        return 'Avansert';
        break;
      case 900:
        return 'Doktorgrad';
        break;
      default:
        return 'Ikke spesifisert';
    }
  } else {
    return 'Ikke spesifisert';
  }
}

module.exports = mongoose.model('Subject', SubjectSchema);
