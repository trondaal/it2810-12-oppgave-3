let Subject = require('../models/subject.js');
let fetch = require('node-fetch');
let URL = 'http://www.ime.ntnu.no/api/course/'
let mongoose = require('mongoose');
let config = require('./database.js')

mongoose.connect(config.database);

fetch(URL + '-')
  .then((res) => {
    let json = res.json();
    if(res.status >= 200 && res.status < 300) {
      return json;
    } else {
      return json.then(err => {throw err;});
    }
  }).then((json) => {
    let courses = []
    json.course.map((key) => {
      courses.push(key.code);
    })
    return courses;
  }).then((courses) => {
    courses.map((code) => {
      fetch(URL + code).then((res) => {
        let json = res.json();
        if(res.status >= 200 && res.status < 300) {
          return json;
        } else {
          return json.then(err => {throw err;});
        }
      }, {concurrency: 2}).then((json) => {
        let data = json.course;
        Subject.findOne({
          code: data.code
        }, (err, subject) => {
          if (err) {
            console.log("Err", err);
          }
          if (!subject) {
            let newSubject = new Subject({
              code: data.code,
              name: data.norwegianName,
              credit: data.credit,
              studyLevel: data.studyLevelCode,
              taughtInEnglish: data.taughtInEnglish,
              taughtInAutumn: data.taughtInAutumn,
              taughtInSpring: data.taughtInSpring,
              location: data.location
            });

            newSubject.save((err) => {
              if(err) {
                console.log("Error", err);
              } else {
                console.log("Saved", newSubject.code);
              }
            });
          }
        });
      }).catch((err) => {
        console.log("Error fetching:", err);
      });
    });
  })
  .catch((err) => {
    console.log("Parsing failed", err);
  });
