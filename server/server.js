var express = require('express');
var server = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var port = process.env.PORT || 3000;
var passport = require('passport');
var path = require('path');
var router = express.Router();
var config = require('./config/database.js');
var mongoose = require("mongoose");
var User = require('./models/user.js');
var Subject = require('./models/subject.js');
var jwt = require('jsonwebtoken');


server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use(morgan('dev'));
server.use(passport.initialize());

mongoose.connect(config.database);

require("./config/passport.js")(passport);

generateToken = function(user) {
  return jwt.sign(user, config.secret, { expiresIn: '24h'});
}

server.post('/api/createuser', function(req, res) {

  if(!req.body.email || !req.body.password || !req.body.fullname) {
    return res.json({success: false, msg: "Please pass inn name, email and password!"});
  } else {
    var newUser = new User({
      email: req.body.email,
      password: req.body.password,
      name: req.body.fullname
    })

    newUser.save(function(err) {
      if(err) {
        return res.json({success: false, msg: "User already exists."});
      }

      return res.json({success: true, msg: "User successfully created", token: generateToken(newUser)});
    })

  }
});

server.post('/api/authenticate', function(req, res) {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) {
      throw err;
    }
    if(!user) {
      res.send({success: false, msg: "User not found!"})
    } else {
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          var token = generateToken(user);
          res.json({success: true,
            user: {
              email: user.email,
              name: user.name,
              favorites: user.favorites
            },
            token: 'JWT' + token});
        } else {
          res.json({success: false, msg: "Wrong password"});
        }
      });
    }
  });
});

server.post('/api/reauthenticate/', passport.authenticate('jwt', {session: false}) ,function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    try {
      var decode = jwt.verify(token, config.secret);
    } catch (err) {
      return res.status(403).send({success: false, msg: "Invalid token"})
    }

    user = decode._doc
    User.findOne({
      id: user._id
    }, function(err, user) {
      if(err) {
        console.log(err);
        return res.status(403).send({success: false, msg: err});
      }

      if (!user) {
        return res.status(403).send({success: false, msg: "User not authenticated!"});
      } else {
        res.json({success: true,
          user: {
            email: user.email,
            name: user.name,
            favorites: user.favorites
          }});
      }
    })
  }
});

server.put('/api/favorite', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  if(token && req.body.subjectCode) {
    try {
      var decode = jwt.verify(token, config.secret);
    } catch (err) {
      return res.status(403).send({success: false, msg: "User not authenticated"});
    }

    user = decode._doc;
    User.findById({
      _id: user._id
    }, function(err, user) {
      if(err) {
        console.log(err);
        return res.status(403).send({success: false, msg: err});
      }
      if(!user) {
        return res.status(403).send({success: false, msg: "User not found"});
      } else {
        subject = Subject.findOne({code: req.body.subjectCode}, function(err, subject) {
          if (err) {
            res.status(400).send({success: false, err: err});
          }

          if (!subject) {
            return res.status(400).send({success: false, msg: "Subject does not exist"});
          } else {
            if (user.favorites.indexOf(subject._id) > -1) {
              return res.json({success: false, msg: "Subject is already a favorite"});
            } else {
              user.favorites.push(subject._id);
              user.save(function(err) {
                if (err) {
                  res.send(err);
                }
                return res.json({success: true, msg: "Favorite saved successfully"});
              });
            }
          }
        });
      }
    });
  }
});

server.delete('/api/favorite', function(req, res) {
  var token = getToken(req.headers);
  if(token && req.body.subjectCode) {
    try {
      var decode = jwt.verify(token, config.secret);
    } catch (err) {
      return res.status(403).send({success: false, msg: "User not authenticated"});
    }

    user = decode._doc;
    User.findById({
      _id: user._id
    }, function(err, user) {
      if(err) {
        console.log(err);
        return res.status(403).send({success: false, msg: err});
      }
      if(!user) {
        return res.status(403).send({success: false, msg: "User not found"});
      } else {
        subject = Subject.findOne({code: req.body.subjectCode}, function(err, subject) {
          if (err) {
            res.status(400).send({success: false, err: err});
          }

          if (!subject) {
            return res.status(400).send({success: false, msg: "Subject does not exist"});
          } else {
            if (user.favorites.indexOf(subject._id) < 0) {
              return res.json({success: false, msg: "Subject is not a favorite"});
            } else {
              var index = user.favorites.indexOf(subject._id);
              user.favorites.splice(index, index + 1);
              user.save(function(err) {
                if (err) {
                  res.send(err);
                }
                return res.json({success: true, msg: "Favorite deleted successfully"});
              });
            }
          }
        });
      }
    });
  }
})

server.get('/api/favorite', function(req, res) {
  var token = getToken(req.headers);
  if(token) {
    try {
      var decode = jwt.verify(token, config.secret);
    } catch (err) {
      return res.status(403).send({success: false, msg: "User not authenticated"});
    }

    user = decode._doc;
    User.findById({
      _id: user._id
    }).populate('favorites')
    .exec(function(err, user) {
      if (err) {
        res.status(400).send({success: false, err: err});
      }
      res.json({
        success: true,
        user: user
      });
    });
  }
});

getToken = function(headers) {
  if (headers && headers.authorization) {
    var parts = headers.authorization.split(' ');
    if (parts.length == 2) {
      return parts[1]
    }
  }
  return null
}


server.get('/api/course', function(req, res) {
  Subject.find({}, function(err, subjects) {
    if(err) {
      return res.json({success: false, msg: "Error"});
    }
    resSubjects = {};
    subjects.map(function(subject) {
      resSubjects[subject.code] = {
        name: subject.name,
        credit: subject.credit,
        levelCode: subject.studyLevel,
        level: subject.calcStudyLevel(),
        taught: {
          spring: subject.taughtInSpring,
          autumn: subject.taughtInAutumn
        },
        english: subject.taughtInEnglish,
        location: subject.location
      };
    });
    res.json(resSubjects);
  })
});

server.get('/api/course/:code', function(req, res) {
  Subject.findOne({
    code: req.params.code
  }, function(err, subject) {
    if (err) {
      console.log(err);
      return res.json({success: false, Error: "An error occured, try again!"});
    }

    if (!subject) {
      return res.json({success: false, msg: "Invalid subject, please try again!"});
    } else {
      return res.json({
        code: subject.code,
        name: subject.name,
        credit: subject.credit,
        levelCode: subject.studyLevel,
        level: subject.calcStudyLevel(),
        taught: {
          spring: subject.taughtInSpring,
          autumn: subject.taughtInAutumn
        },
        english: subject.taughtInEnglish,
        location: subject.location
      });
    }
  });
});

console.log("Whiskey at port " + port);
server.listen(port);
