import React from 'react';

//Child component to CourseView
class CourseDetails extends React.Component {
  constructor(props) {
    super(props);
    this.getTerm = this.getTerm.bind(this);
    this.inEnglish = this.inEnglish.bind(this);
  }

  //render a li tag with the coursedata
  render() {
    return (
      <div className="CourseDetails">
        <div className="topInfo">
          <div className="leftInfo">
            <p><strong>Studiepoeng: </strong>{this.props.course.credit}sp</p>
            <p><strong>Undervisningstermin: </strong>{this.getTerm()}</p>
          </div>
          <div className="rightInfo">
            <p><strong>Engelsk: </strong>{this.inEnglish()}</p>
            <p><strong>Lokasjon: </strong>{this.props.course.location}</p>
          </div>
        </div>
      </div>
    );
  } 

  getStudiepoeng(code){
      //props.credit
      return 100;
  }
  getTerm(){
    var spring = this.props.course.taught.spring;
    var autumn = this.props.course.taught.autumn;
    if(spring && autumn){
      return "Vår/Høst";
    }
    else if(spring){
      return "Vår"
    }
    else if (autumn){
      return "Høst"
    }
    else{
      return "-"
    }
  }
  
  inEnglish(){
    if(this.props.course.english){
      return "Ja";
    }
    else{
      return "Nei";
    }
  }

}

export default CourseDetails;
