import React from 'react';
import CourseView from './CourseView.jsx';

class MySite extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            favourites: [{"code": "TMA4140", "name": "Diskret matematikk" }, {"code": "IT2810", "name": "Webutvikling" }]
        }
    }

    render (){
        return(
            <div id="favouriteList">
                <h2>My Favourite Subjects</h2>
                <div id="favouriteView">
                    <CourseView subjects={this.state.favourites}/>
                </div>
            </div>
            )
    }

    componentDidMount(){
        //TODO: get favourites from database, using user id
        this.setState({favourites: [...this.state.favourites, "this is a subject"]});
    }
}

export default MySite
