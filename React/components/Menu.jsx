import React from 'react';
import Auth from '../modules/Auth';
import {Link} from 'react-router';

class Menu extends React.Component {

    constructor(props){
        super(props);
    }

    render (){
        return(
            <div>
                  { Auth.isUserAuthenticated() ? (
                    <div className="menu">
                      <Link to="/"><button>Home</button></Link>
                      <Link to="/mysite"><button>My Page</button></Link>
                      <Link><button onClick={Auth.deauthenticateUser}>Log out</button></Link>
                      <Link to="/wordcloud"><button>Word Cloud</button></Link>
                    </div>
                  ) : (
                    <div className="menu">
                      <Link to="/"><button>Home</button></Link>
                      <Link to="/login"><button>Log in</button></Link>
                      <Link to="/wordcloud"><button>Word Cloud</button></Link>
                    </div>
                  )}
                <h1>FagGuiden!</h1>

                {React.cloneElement(this.props.children)}

            </div>
            )
    }

    /*login(){
        //TODO: fix user login
        const userid = "12345";
        this.setState({personId: userid});
    }*/
}

export default Menu
