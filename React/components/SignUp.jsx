import React from 'react';
import "whatwg-fetch";
import Auth from '../modules/Auth';


class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      name: "",
      message: ""
    }

    this.submitUser = this.submitUser.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.handleName = this.handleName.bind(this);
  }

  render() {
    return (
      <div>
        <h2>Registrer</h2>
        <form onSubmit={this.submitUser}>
          <input type="email" placeholder="E-post" onChange={this.handleEmail}></input>
          <input type="password" placeholder="Passord" onChange={this.handlePassword}></input>
          <input type="text" placeholder="Name" onChange={this.handleName}></input>
          <button type="submit">Send</button>
        </form>
      </div>
    )
  }

  submitUser() {
    console.log("Hei")
    console.log(this.state.email)
    if(this.state.email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) && this.state.password != "") {
      console.log("Heihei")
      this.setState({
        message: "Bruker opprettet"
      });

      var credentials = {
        email: this.state.email,
        password: this.state.password,
        fullname: this.state.name
      }
      fetch('http://it2810-12.idi.ntnu.no:3000/api/createuser', {
        headers: {
          'Content-Type': "application/json; charset=UTF-8"
        },
        method: 'POST',
        body: JSON.stringify(credentials)
      }).then((response) => {
          return response.json();
        }).then((json) => {
          console.log(json)
          if(json.success) {
            Auth.authenticateUser(json.token);
          } else {
            this.setState = {
              message: "Brukeren finnes allerede"
            }
          }
        })
      } else {
      this.setState({
        message: "Vennligst oppgi en gyldig e-post"
      });
    }
  }

  handleEmail(event) {
    this.setState({
      email: event.target.value
    })
  }

  handlePassword(event) {
    this.setState({
      password: event.target.value
    })
  }

  handleName(event) {
    this.setState({
      name: event.target.value
    })
  }
}

export default SignUp
