import React from 'react';

//A FilterCategory element represents and displays one filter "genre", e.g. Education Language.
//The options represent the filter criteria for that genre, e.g. ["Norwegian", "English"].
//These are generated as distinct input checkboxes in the render function, 
//and their value is bound to the appropriate object in checkedFilters of Formview
class FilterCategory extends React.Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <div className="filterBlock">
                <span>{this.props.title}</span>
                {this.props.options.map((option) =>{
                    //Sets value of input element to float if filter is for credit points, else sets to lowercase string
                    //(for easier matching with courses later)
                    let value;
                    if (this.props.name == "credit"){
                        value = parseFloat(option);
                    }
                    else {
                        value = option.toLowerCase();
                    }
                    let lowerOptions = option.toLowerCase();
                    return ( <div key={option}><label>{option}</label><input type="checkbox" value={value} name={this.props.name} checked={this.props.checkboxStatus[lowerOptions]} onChange={this.props.onChange} /></div>);
                })}
            </div>);
        }
}

export default FilterCategory;