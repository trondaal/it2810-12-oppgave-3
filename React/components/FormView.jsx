import React from 'react';
import FilterCategory from './FilterCategory.jsx';
//Component whose purpose is to generate FilterCategory elements, manage their states,
// and serve as an interface-ish bridge towards the SearchEngine component

class FormView extends React.Component{

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.updateFilters = this.updateFilters.bind(this);
       
        this.state = {
            //Objects for each filter category. The values list is dynamic, and contains the currently checked filters of each category
            checkedFilters: [
                {category: "credit", values:[]},
                {category: "taught", values:[]},
                {category: "language", values:[]},
                {category: "location", values:[]},
                {category: "level", values:[]}
                ],
            //Container two-way binding the state of each filter checkboxes
            checkboxStatus: {
                "7.5": false,
                "10": false,
                "15": false,
                "spring": false,
                "autumn": false,
                "english": false,
                "gjøvik": false,
                "trondheim": false,
                "ålesund": false,
                "basic": false,
            }
        };

        //Static info needed to generate filter categories for rendering
        this.filterCategories = [
            {title: "Credits", name: "credit", options: ["7.5", "10", "15"]},
            {title: "Term", name: "taught", options: ["Spring", "Autumn"]},
            {title: "Language", name: "language", options: ["English"]},
            {title: "Location", name: "location", options: ["Trondheim", "Ålesund", "Gjøvik"]},
            {title: "Level", name: "level", options: ["Basic"]}
            ];
        }
        //Extracts data from an on-change event and updates the binding state with new select-state
        handleChange(event){
            let wasChecked = event.target.checked;
            let filterproperty = event.target.value;
            let category = event.target.name;
            var checkboxStatus = this.state.checkboxStatus;
            let valueString = filterproperty.toString();
            checkboxStatus[valueString] = wasChecked;
            //Updates state
            this.setState({
                checkboxStatus: checkboxStatus
            });
            //Calls updateFilter() of SearchEngine
            this.updateFilters(filterproperty, category, wasChecked);
        }

        // Modifies this.state's checkedFilters array
        // @param {string} filterProperty - The value of the checkbox that has been (de)selected
        // @param {string} category - the category that the clicked filter belongs to, e.g. "term"
        // @param {boolean} isChecked - Boolean indicating whether that filter has been checked or unchecked
       
        updateFilters(filterProperty, category, isChecked){
            var currentFilters = this.state.checkedFilters;
            var objectindex = -1;
            //Finds the index in checkedFilters that holds the category that has been filtered
            currentFilters.forEach(function(element) {
                if (element.category == category){
                    objectindex = currentFilters.indexOf(element);
                }}, this);
            //If a filter was checked, we push the value to the list of the appropriate category
            if (isChecked) {
           
                currentFilters[objectindex].values.push(filterProperty);
            }
            //If unchecked, we remove that value from the category to un-filter it
            else {
                currentFilters[objectindex].values = currentFilters[objectindex].values.filter((filterValue) => {
                    return filterValue != filterProperty;
                });
            }
            //Updating state
            this.setState({
                checkedFilters: currentFilters
                }); 
            //Calling filterCourses() of SearchEngine to use the newly modified filter list to do the actual filtering
            this.props.onChange(this.state.checkedFilters);

        }


    render () {
        return (
            <ul>
                {this.filterCategories.map((category) => {
                    return (<FilterCategory key={category.title} title={category.title} checkboxStatus={this.state.checkboxStatus} onChange={this.handleChange} name={category.name} options={category.options}/>);
                })}
            </ul>
        );
    }
}

export default FormView;