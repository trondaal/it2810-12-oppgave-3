import React from 'react';

//This class creates a random wordcloud from a list of words and values

class Wordcloud extends React.Component {

    constructor(props){
        super(props);
        //binds all the necessary functions
        this.getHighestValue = this.getHighestValue.bind(this);
        this.placeWords = this.placeWords.bind(this);
        this.overlaps = this.overlaps.bind(this);
        this.showDescription = this.showDescription.bind(this);
        //the state, containing all the words, and their associated values
        this.state = {
            topSubjects: [
                //test-data.. remove these
                ["TDT1234", 4],
                ["TDT4242", 5],
                ["TDT9001", 12],
                ["TMA4000", 25],
                ["FUGL123", 40],
                ["EXPH420", 12],
                ["IT1337", 30],
                ["TES1234", 4],
                ["TES4242", 5],
                ["TES9001", 12],
                ["TES4000", 25],
                ["TES123", 40],
                ["TES420", 12],
                ["TES1337", 30],
            ],
        }
    }

    render (){
        //this factor is used to make the largest word appear at 100px, no matter what the highest value is
        const high = this.getHighestValue();
        const factor = 100/high;
        //Every word rendered is given a size according to their value, and a random color
        return(
            <div>
                <ul id="wordcloud">
                    {this.state.topSubjects.map((subject) => {
                        const size = factor*subject[1]+"px";
                        const color = "#"+(Math.random()*0xFFFFFF<<0).toString(16);
                        return (<li className="word"
                                    id={subject[0]}
                                    key={subject[0]}
                                    style={{fontSize: size, color: color}}
                                    onMouseMove={(event) => this.showDescription(subject[1], event)}> {subject[0]}</li>);
                    })}
                    <div id="wordPopup"></div>
                </ul>
            </div>
        )
    }

    //collects the data from the server, and places the words, after the component has mounted
    componentDidMount(){
        //TODO: get favourites from database, using user id
        //TODO: slice the data
        //this.setState({subjects: [...this.state.favourites, "this is a subject"]});

        //sorts the words according to their values
        this.setState({
            topSubjects: this.state.topSubjects.sort((a,b) => {
                return b[1] - a[1];
            }),
        });
        this.placeWords();
    }

    //function for giving the coordinates of each word
    placeWords(){
        //the coordinates for the largest word (the middle of the cloud)
        const xPos = window.innerWidth/2;
        const yPos = window.innerHeight/2 + 60;
        //iterates through every word
        this.state.topSubjects.map((subject) => {
            const word = document.getElementById(subject[0]);
            const size = word.getBoundingClientRect();
            const xOffset = size.width/2;
            const yOffset = size.height/2;
            word.style.position = "absolute";
            let radius = 0;
            let radians = 0;
            let readyToPlace = false;
            while (!readyToPlace){
                readyToPlace = true;
                //places the word on some coordinates in an oval shape
                word.style.left = xPos-xOffset+radius*1.8*Math.cos(radians)+"px";
                word.style.top = yPos-yOffset+radius*Math.sin(radians)+"px";
                //check if the word overlaps with any other word
                this.state.topSubjects.map((other) => {
                    if (this.overlaps(subject[0],other[0])){
                        readyToPlace = false;
                    }
                });
                //increases the radius of the oval, in case the word overlapped another
                radius += 1;
                //finds a random radian, so the wordcloud becomes random
                radians = Math.random() * (2*Math.PI);
            }
        })
    }

    //makes the description of a word follow the mouse
    showDescription(value, e){
        const popup = document.getElementById("wordPopup");
        popup.style.left = e.clientX - 110 + 'px';
        popup.style.top = e.pageY - 25 + 'px';
        popup.innerHTML = "Favourited: " + value;
    }

    //checks if two element are overlapping
    overlaps(wordOne, wordTwo){
        if (wordOne === wordTwo){
            return false;
        }
        const one = document.getElementById(wordOne).getBoundingClientRect();
        const two = document.getElementById(wordTwo).getBoundingClientRect();
        return !(one.right < two.left || one.left > two.right || one.bottom < two.top || one.top > two.bottom);
    }

    //returns the highest value in the list of words
    getHighestValue(){
        let highestValue = this.state.topSubjects[0][1];
        this.state.topSubjects.map((subject) => {
            if (subject[1]>highestValue){
                highestValue = subject[1]
            }
        });
        return highestValue;
    }
}

export default Wordcloud
