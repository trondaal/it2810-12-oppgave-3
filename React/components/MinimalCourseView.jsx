import React from 'react';
import CourseDetails from './CourseDetails.jsx';
import Favourites from "./Favourites.jsx";

//Child component to CourseView
class MinimalCourseView extends React.Component {
  constructor(props) {
    super(props);
    this.expand = this.expand.bind(this);
    this.state = {
        showDetails: false,
      }
  }

  //render a li tag with the coursedata
  render() {
    return (
      <div>
      <li className="resultTitle">
        <span onClick={this.expand}>{this.props.course.code} - {this.props.course.name}</span> { this.props.login ? <Favourites style="display:inline" code={this.props.course.code} name={this.props.course.name} />: null }
         { this.state.showDetails ? <CourseDetails course={this.props.course}/> : null }
      </li>
      </div>
    );
  }

      expand(){
      this.setState({ showDetails: !this.state.showDetails });
      console.log("state: " + this.state.showDetails);
    }

}

export default MinimalCourseView;
