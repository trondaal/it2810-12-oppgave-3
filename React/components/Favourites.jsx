import React from "react";


class Favourites extends React.Component {
  constructor(props) {
    super(props);
    this.addFavourite = this.addFavourite.bind(this);
    this.removeFavourite=this.removeFavourite.bind(this);
    this.state = {
      imageSource : "img/starOutline.png"
    }
  }

//Render a star which can be used to mark favourite courses.
  render() {
    return (
        <img src={this.state.imageSource} className="favIcon" onClick={this.addFavourite}/>
    );
  }
  //adds the subject to the database. The option should only be available when the user have logged in.
  //The method also deletes the subject if it already exist
  addFavourite(){

      if(this.state.imageSource === "img/starFull.png"){
        //TODO: delete request should be placed here

          this.setState({imageSource:"img/starOutline.png"}, function () {
          });
      }else {
      this.setState({
        //TODO: put request should be placed here
        
        imageSource: "img/starFull.png"
      });}
  }

}

export default Favourites;
