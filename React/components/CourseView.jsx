import React from 'react';
import MinimalCourseView from './MinimalCourseView.jsx';

class CourseView extends React.Component {
  constructor(props) {
    super(props);
  }

  //Render a ul with MinimalCourseView components mapped by props
  render() {
    return (
      <div className="rullebane extraPadding">
        <ul>
          {this.props.subjects.map((subject) => {
            return(<MinimalCourseView user={this.props.user} course={subject} key={subject.code} login={this.props.login}/>  );
          })}
        </ul>

        </div>
    );
  }

}

export default CourseView;
