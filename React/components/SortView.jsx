import React from 'react';
import MinimalCourseView from './MinimalCourseView.jsx';

class SortView extends React.Component {
  constructor(props) {
    super(props);
    this.sortByValue = this.sortByValue.bind(this);
  }

  //Render a ul with MinimalCourseView components mapped by props
  render() {
    return (
      <div>
        <button value="code" onClick={this.sortByValue}>Sort by code</button>
        <button value="name" onClick={this.sortByValue}>Sort by name</button>
      </div>
    );
  }

  //Method sorts results list based on input value
    sortByValue(event){
      var sortParam = event.target.value;
      var subjects = this.state.subjects
      subjects.sort((a, b) => {
        if(a[sortParam] > b[sortParam]) {return 1;}
        if(a[sortParam] < b[sortParam]) {return -1;}
        return 0;
      })
      this.setState({
        subjects: subjects,
        viewRange: subjects
      })
    }

}

export default SortView;
