import React from 'react';
import "whatwg-fetch";
import Auth from '../modules/Auth';
import SignUp from './SignUp.jsx'

class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      message: ""
    }

    this.submitUser = this.submitUser.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
  }

  render() {
    return (
      <div>
        <h2>Logg inn</h2>
        <form onSubmit={this.submitUser}>
          <input type="email" placeholder="E-post" onChange={this.handleEmail}></input>
          <input type="password" placeholder="Passord" onChange={this.handlePassword}></input>
          <button type="submit">Send</button>
        </form>
        <SignUp />
      </div>
    )
  }

  submitUser() {
    console.log("Hei")
    console.log(this.state.email)
    if(this.state.email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) && this.state.password != "") {
      console.log("Heihei")
      this.setState({
        message: "Bruker opprettet"
      });

      var credentials = {
        email: this.state.email,
        password: this.state.password
      }
      fetch('http://it2810-12.idi.ntnu.no:3000/api/authenticate', {
        headers: {
          'Content-Type': "application/json; charset=UTF-8"
        },
        method: 'POST',
        body: JSON.stringify(credentials)
      }).then((response) => {
          return response.json();
        }).then((json) => {
          console.log(json)
          if(json.success) {
            Auth.authenticateUser(json.token);
          } else {
            this.setState = {
              message: "Brukeren finnes allerede"
            }
          }
        })
      } else {
      this.setState({
        message: "Vennligst oppgi en gyldig e-post"
      });
    }
  }

  handleEmail(event) {
    this.setState({
      email: event.target.value
    })
  }

  handlePassword(event) {
    this.setState({
      password: event.target.value
    })
  }
}

export default LogIn
