import React from 'react';
//Fetch module for easy AJAX handling
import 'whatwg-fetch';
import CourseView from './CourseView.jsx';
import FormView from './FormView.jsx';
import Auth from '../modules/Auth';

// NOTE: For the filter functions to work properly, the app requires course objects with extended properties
//These properties are currently: credit, educationLanguage, and term.
//To run a simple node express app (myapp) serving such mock-up data:
//   Open a new terminal (cmd for windows), navigate to 'myapp' in the project , maybe run npm install, and then run command 'node index.js'
//the url at the bottom of this component (localhost:3000) should now work, and this app should receive the mockup courses


import Favourites from "./Favourites.jsx";


class SearchEngine extends React.Component {

    //Pretty big constructor for the main component
    constructor(props) {
      super(props);
      //Need to bind this to the methods
      this.handleChange = this.handleChange.bind(this);
      this.httpGetJSON = this.httpGetJSON.bind(this);
      this.findMatchingSubjects = this.findMatchingSubjects.bind(this);
      this.handleScrolling = this.handleScrolling.bind(this);
      this.filterCourses = this.filterCourses.bind(this);
      this.sortByValue = this.sortByValue.bind(this);
      this.compare = this.compare.bind(this);

      //Basic state values for the main component, sliceRange represents number of subjects shown
      //checkedFilterCategories contains an object for each filtercategory that has been selected
      this.state = {

        searchInput: "",
        subjects: [],
        viewRange: [],
        sliceRange: 15,
        login: Auth.isUserAuthenticated()
      }
    }
    render() {
        //Render a div with an inputfield, a formview element (list of filters), and a list with the courses
        //The list is a CourseView component with the courses as props, sliced to the target length
        return (
            <div className="mainContent">
            <div id="searchOrg">
              <div id="sortButtons">
                <h4>SORTER PÅ:</h4>
                <button value="code" onClick={this.sortByValue}>Kode</button>
                <button value="name" onClick={this.sortByValue}>Navn</button>
              </div>
              <div id="filterButtons">
              <h4>FILTRER PÅ:</h4>
              <FormView onChange={this.filterCourses}/>
              </div>
            </div>
            <div id="searchEngine">
              <input id="searchBar" className="rullebane" type="text" ref="fagid" placeholder="Search for courses" onChange={this.handleChange}></input>
              <CourseView user={this.props.user} subjects={this.state.viewRange.slice(0,this.state.sliceRange)} login={this.state.login} />
            </div>
            </div>

        );
    }


     // Modifies the viewRange (displayed courses) according to the checkedFilters list argument
     filterCourses(checkedFilters) {
       const allCourses = this.state.subjects;
        const currentFilters = checkedFilters;
        const filterCategoriesLength = currentFilters.length;
        //We iterate through each course in the complete course list, and filter out those who do not meet the required criterias (those which return includeThisCourse = false)
        var resultCourseList = allCourses.filter((course) => {
          var includeThisCourse = true;
          //Iterates through each filtercategory
          for (let i=0; i < filterCategoriesLength; i++){
            let filterCategory = currentFilters[i];
            let selectedValuesLength = filterCategory.values.length;
            //This if statement is only entered when there is a selected filter within this category
            if(selectedValuesLength > 0){
              let categoryName = filterCategory.category;
              let selectedValues = filterCategory.values;
              //If there is only a single selected filter within this category, the course is dropped if there's no match
              // if there is a match, we set update includeThisCourse and the for loop continues to the next filter category
              if(selectedValuesLength == 1){
                var match = this.compare(course, categoryName, selectedValues[0]);
                if (!match){
                  includeThisCourse = false;
                  break;
                }
                includeThisCourse = match;
              }
              //When there are multiple filter criterias on the same category, e.g. both "English" AND "Norwegian",
              //we do an OR ish function and include the course if it matches either one
              else{
                var match = false;
                for (let k=0; k < selectedValuesLength; k++){
                  if(this.compare(course, categoryName, selectedValues[k])){
                    match = true;
                    break;
                  }
                }
                if(!match){
                  includeThisCourse = false;
                  break;
                  }
                }
              }
            }
            return includeThisCourse;
          });
          //Updating state
          this.setState({
          viewRange: resultCourseList
        });
      }
    //Compares a filtered property to a course.*
    //Returns true if the course has the desired property for filtering
    compare(course, categoryName, selectedValue){
      if (categoryName == "credit") {
        return course.credit == selectedValue;
      }
      else if(categoryName == "taught"){
        return course.taught[selectedValue];
      }
      else if(categoryName == "language"){
        return course[selectedValue];
      }
      else if(categoryName == "location"){
        return course.location.toLowerCase() == selectedValue;
      }
      else{ //Else, category is level
        return course.level == "Grunnleggende";

      }
    }


    //Method sorts results list based on input value
    sortByValue(event){
      console.log("user:" + this.props.user);
      var sortParam = event.target.value;
      var subjects = this.state.subjects
      subjects.sort((a, b) => {
        if(a[sortParam] > b[sortParam]) {return 1;}
        if(a[sortParam] < b[sortParam]) {return -1;}
        return 0;
      })
      this.setState({
        subjects: subjects,
        viewRange: subjects
      })
    }

    //Method that is called everytime you change the text in the inputfield
    handleChange(event) {
      //Changes the state of inputvalue, the array of matching subjets and resets the slice range
      this.setState({
        searchInput: event.target.value,
        viewRange: this.findMatchingSubjects(event.target.value),
        sliceRange: 15
      });
    }

    //Method for dynamic load og new subjects when scrolling downwards
    handleScrolling(event) {
      if(window.innerHeight + document.body.scrollTop >=
        document.body.offsetHeight) {
          //Add 10 to the slice range when reaching bottom
          this.setState({
            sliceRange: this.state.sliceRange += 10
          })
        }
    }

    //Method for the search
    findMatchingSubjects(search) {
      //Check if the search input is empty and if true, we load the entire list of subjects
      if(search === "") {
        this.setState({
          viewRange: this.state.subjects
        });
        //If search input, we loop through the array of subjects and look for matches
        //If match, we put the matching subject in a new temporary array
      } else {
        var newRange = [];
        this.state.subjects.map((subject) => {
          if(subject.code.toLowerCase().search(search.toLowerCase())
           != -1 || subject.name.toLowerCase().search(search.toLowerCase()) != -1) {
             newRange.push(subject);
           }
        })
        //Return the new array with matching subjects
        return newRange;
      }
    }


    //Simple AJAX request with the fetch-module
    httpGetJSON(url) {
      //Uses promises syntax, first fetch the data from url
      fetch(url)
      //then parse the data to json
        .then((response) => {
          return response.json()
          //then loop through the data and create objects in the final array with subject code and name
        }).then((json) => {
          var final = [];
          for (var key in json){;
            final.push({code: key, name: json[key].name, level: json[key].level, taught: json[key].taught, english: json[key].english, credit: json[key].credit, location: json[key].location});
            
          }

          console.log(final[0]);
          
          
          //Then sort the final array by subject code
          final.sort((a, b) => {
            if(a.code > b.code) {return 1;}
            if(a.code < b.code) {return -1;}
            return 0;
          })
          //Sets the subjectstate to the final array
          this.setState({subjects: final});
          //call findmatchingsubjects with no input
          this.findMatchingSubjects("");
        //Exeption handling
        }).catch((ex) => {
          console.log('parsing failed', ex)
        });
    }

    //Pre built method that run when the component successfully mounts to the DOM
    componentDidMount() {
      //Add a scrolling listener and send a request to the api
      window.addEventListener('scroll', this.handleScrolling);
      this.httpGetJSON("http://it2810-12.idi.ntnu.no:3000/api/course/");


    }

    //If the component will unmount from the DOM, we close the scrolling listener
    componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScrolling);
    }
}

export default SearchEngine;
