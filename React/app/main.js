import React from 'react';
import ReactDOM from 'react-dom';
import Menu from '../components/Menu.jsx';
import SearchEngine from '../components/SearchEngine.jsx';
import MySite from '../components/MySite.jsx';
import Wordcloud from '../components/Wordcloud.jsx';
import LogIn from '../components/LogIn.jsx';
import {Router, Route,IndexRoute, IndexLink, Link, hashHistory} from 'react-router';



ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Menu}>
            <IndexRoute component={SearchEngine}/>
            <Route path="mysite" component={MySite}/>
            <Route path="wordcloud" component={Wordcloud}/>
            <Route path="login" component={LogIn}/>
        </Route>
    </Router>,
    document.getElementById('content'));
