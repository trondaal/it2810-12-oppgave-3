import 'whatwg-fetch';

class Auth {

  static authenticateUser(token) {
    localStorage.setItem('token', token);
  }

  static isUserAuthenticated() {
    if (localStorage.getItem('token') != null) {
      fetch('http://it2810-12.idi.ntnu.no:3000/api/reauthenticate', {
        method: 'POST',
        headers: {
          'Authorization': localStorage.getItem('token')
        }
      }).then((response) => {
        return response.json;
      }).then((json) => {

        if(json.success == true) {
          return true;
        } else {
          localStorage.removeItem('token');
          return false;
        }
      })
    } else {
      return false;
    }
  }

  static deauthenticateUser() {
    localStorage.removeItem('token');
  }

  static getToken() {
    return localStorage.getItem('token');
  }

}

export default Auth;
